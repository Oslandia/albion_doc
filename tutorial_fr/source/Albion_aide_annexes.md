# Annexes

## Format des données de sondages

### Introduction

Les données d’entrées sont de natures différentes, elles correspondent aussi bien à des données numériques d’enregistrements (exemple de données diagraphique), que des descriptions ou commentaires de données géologiques.
Les fichiers d’entrées sont des fichiers Ascii dont les noms, extensions et les formats de champs sont normalisés afin d’automatiser l’entrée des données. Cinq catégories de données ont été distinguées. Elles sont présentées dans la figure ci-dessous. Chaque catégorie peuvent contenir différentes tables qui sont décrites dans cette note.

Résumé des différents types de données nécessaires pour visualiser en carte, coupes et 3D des données de sondages avec Albion :

![image](_static/image142.png)


### Collar

La table `collar` correspond à la localisation X,Y,Z de la tête de sondage sur la surface topographique dans le système de projection indiqué par le modélisateur dans QGIS. Le fichier tête de sondage est unique, chaque sondage est défini par son nom `holeid` qui lui aussi est unique.

Description du fichier `collar` (en rouge données obligatoires en bleu, données facultatives) :

![image](_static/image143.png)


### Déviation

La géométrie du sondage sera définie à partir des données de déviation. Le fichier `déviation` correspond à l’enregistrement pente et azimut du sondage, pour un intervalle donné. Ce fichier avec le fichier `collar` permet de définir en coordonnées cartésienne la représentation spatiale du sondage.

Description du fichier `déviation` (en rouge données obligatoires en bleu, données facultatives) :

![image](_static/image144.png)

Représentation en coupe d’un sondage dévié :

![image](_static/image145.png)

### Calcul des coordonnées des passes de sondages

C’est à partir du fichier déviation et du fichier collar que sont calculés les paramètres FromX,FromY,FromZ , ToX,ToY,ToZ (en vert dans les chapitres suivants). Ces paramètres sont nécéssaire pour la représentation en coupe les données de sondages. La méthode utilisée pour calculer les coordonnées des passes à partir des données de profondeur et de la position de la tête de sondage est présenté ci-dessous est la `balanced tangential methode`.

### Radiométrie

La mesure dont on dispose traditionnellement correspond à un enregistrement tous le 10cm de la mesure gamma elle est enregistrée à l’aide de sondes radiométrique divers (NGRS, GT etc…)

Description du fichier « radiométrie » (en rouge, les données obligatoires, en vert les données calculées par Albion) :

![image](_static/image146.png)


### Résistivité

La mesure dont on dispose traditionnellement correspond à un enregistrement tous les 10cm mesure avec une sonde de résistivité.

Description du fichier « résistivité »(en rouge données obligatoires en vert les données calculées par Albion) :

![image](_static/image147.png)

### Formation

La table formation permet de décrire le long des sondages les formations géologiques reconnues par le géologue lors de la description de cuttings ou de carottes. Les différentes formations intersectées sont codifiées (numérique) avec un champ texte permettant des observations complémentaires sur les passes codées identifiées.


Description du fichier « formation » (en rouge les données obligatoires, en vert les données calculées par Albion, en bleu les données facultatives)

![image](_static/image148.png)

### Lithologie

La table lithologie décrit les différentes lithologies intersectées lors de la foration. Les lithologies sont codifiées (numérique). Un champ texte permet de complété ces observations par une description naturaliste de la roche.

Description du fichier « lithologie » (en rouge les données obligatoires, en vert les données calculées par Albion, en bleu les données facultatives)

![image](_static/image149.png)

### Facies

Il s’agit ici de donnée de type facies, de la roche intersectée par sondage. Description du fichier « facies » (en rouge données obligatoire en vert les données calculées par Albion, en bleu données facultatives) :

![image](_static/image150.png)

### Minéralisation

![image](_static/image151.png)

![image](_static/image152.png)

## Construction des sections (séquence mandala)

Nous décrivons ici une ancienne méthode toujours fonctionnelle pour construire les sections dans un modèle géologique, dans la mesure où la methode permet de visualiser plusieurs directions de section, l’approche originale porte le nom de section mandala.

*Le mandala est un support de méditation. Il est le plus souvent représenté en deux dimensions mais on trouve également des mandalas réalisés en trois dimensions. Ce sont des œuvres d’art d’une grande complexité. Le méditant se projette dans le mandala avec lequel il se fond dans le yáng et yīn de la bouddhéité fondamentale. Disposées en plusieurs quartiers, les déités expriment la compassion, la douceur, d’autres l’intelligence, le discernement, d’autres encore l’énergie, la force de vaincre tous les aspects négatifs du subconscient samsarique* (d’après Wikipedia).

Les sections vont mettre de contrôler et de modifier les volumes crées par Albion. Leur géométrie est un gage de qualité dans la construction volumétrique. Cette étape fait appel un travail manuel facile à réaliser une fois que l’on bien compris la problématique. Cette étape peut être assimilée à une scéance de mandala. Dans le cas des données du tutoriel il faut compter 30 minutes pour la réalisation des coupes NS et EW.


TODO copies écran
